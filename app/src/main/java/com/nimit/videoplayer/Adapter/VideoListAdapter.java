package com.nimit.videoplayer.Adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.media.MediaMetadataRetriever;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.Bundle;
import android.os.Parcelable;
import android.provider.MediaStore;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.nimit.videoplayer.R;
import com.nimit.videoplayer.activity.VideoPlayActivity;
import com.nimit.videoplayer.model.VideoInfoModel;

import java.util.ArrayList;
import java.util.Calendar;

public class VideoListAdapter extends RecyclerView.Adapter<VideoListAdapter.ViewHolder> {

    private AppCompatTextView mVideoDuration;
    private AppCompatTextView mVideoTitle;
    private AppCompatImageView mVideoThumbnail;
    private Context context;
    private ArrayList<VideoInfoModel> mVideoList;

    public VideoListAdapter(Context context, ArrayList<VideoInfoModel> mVideoList) {
        this.context = context;
        this.mVideoList = mVideoList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.layout_recylerview_videolist, parent, false);

        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    Intent i = new Intent(context, VideoPlayActivity.class);
                    i.putExtra("videoUrl", mVideoList.get(position).getVideoUrl());
                    context.startActivity(i);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        MediaMetadataRetriever retriever = new MediaMetadataRetriever();
//use one of overloaded setDataSource() functions to set your data source
        retriever.setDataSource(context, Uri.parse(mVideoList.get(position).getVideoUrl()));
        String time = retriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION);
        long timeInMillisec = Long.parseLong(time);
        Calendar cal = Calendar.getInstance();
        cal.setTimeInMillis(timeInMillisec);
        time = cal.getTime().getMinutes() + ":" + cal.getTime().getSeconds();
        mVideoList.get(position).setVideoDuration(time);
        mVideoTitle.setText(mVideoList.get(position).getVideoDisplayName());
        mVideoDuration.setText(time);
        Bitmap bMap = ThumbnailUtils.createVideoThumbnail(mVideoList.get(position).getVideoUrl(), MediaStore.Video.Thumbnails.MICRO_KIND);
        mVideoThumbnail.setImageBitmap(bMap);
    }

    @Override
    public int getItemCount() {
        return mVideoList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public ViewHolder(View itemView) {
            super(itemView);
            mVideoTitle = (AppCompatTextView) itemView.findViewById(R.id.video_title);
            mVideoThumbnail = (AppCompatImageView) itemView.findViewById(R.id.video_thumbnail);
            mVideoDuration = (AppCompatTextView) itemView.findViewById(R.id.video_duration);
        }
    }
}
