package com.nimit.videoplayer.model;

public class VideoInfoModel {
    private String videoUrl;
    private String videoDisplayName;
    private String videoDuration;

    public String getVideoUrl() {
        return videoUrl;
    }

    public void setVideoUrl(String videoUrl) {
        this.videoUrl = videoUrl;
    }

    public String getVideoDisplayName() {
        return videoDisplayName;
    }

    public void setVideoDisplayName(String videoDisplayName) {
        this.videoDisplayName = videoDisplayName;
    }

    public String getVideoDuration() {
        return videoDuration;
    }

    public void setVideoDuration(String videoDuration) {
        this.videoDuration = videoDuration;
    }
}
