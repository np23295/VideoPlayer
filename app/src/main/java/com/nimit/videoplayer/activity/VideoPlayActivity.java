package com.nimit.videoplayer.activity;

import android.media.MediaPlayer;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.MediaController;
import android.widget.ProgressBar;
import android.widget.VideoView;

import com.nimit.videoplayer.R;
import com.nimit.videoplayer.model.VideoInfoModel;

public class VideoPlayActivity extends AppCompatActivity {

    VideoInfoModel mVideo;
    private VideoView videoView;
    private ProgressBar progressBar;
    private String url;

    @Override
    protected void onPause() {
        super.onPause();
        videoView.pause();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_video);
        videoView = (VideoView) findViewById(R.id.videoView);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        progressBar.setVisibility(View.VISIBLE);
        try {
            MediaController controller = new MediaController(VideoPlayActivity.this);
            controller.setAnchorView(videoView);
            url = getIntent().getStringExtra("videoUrl");
            videoView.setMediaController(controller);
            videoView.setVideoURI(Uri.parse(url));

        } catch (Exception e) {
            e.printStackTrace();
        }
        videoView.requestFocus();
        videoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mediaPlayer) {
                progressBar.setVisibility(View.INVISIBLE);
                videoView.start();
            }
        });
    }
}
